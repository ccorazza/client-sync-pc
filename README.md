# Client de synchronisation pour ordinateur ownCloud

| Tâche                             | État                                                                                                                                                              |
|-----------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| client-build-matrix               | [![Build Status](https://ci.owncloud.org/job/client-build-matrix-linux/badge/icon)](https://ci.owncloud.org/job/client-build-matrix-linux/)                       |
| client-test-matrix-linux-no-build | [![Build Status](https://ci.owncloud.org/buildStatus/icon?job=client-test-matrix-linux-no-build)](https://ci.owncloud.org/job/client-test-matrix-linux-no-build/) |
| coverity_scan | [![Build Status](https://img.shields.io/coverity/scan/2482.svg)](https://scan.coverity.com/projects/owncloud-mirall)

## Introduction

Le client pour ordinateur ownCloud est un outil pour synchroniser les fichiers
de votre serveur ownCloud vers votre ordinateur.

## Téléchargement

### Paquets binaires

* Consulter la page de téléchargements https://owncloud.org/install/#install-clients

### Code source

Le développement du client pour ordinateur ownCloud utilise un dépôt Git. Puisque
Git permet facilement de créer des versions dérivées et d'améliorer le code et
de l'adapter à ses besoins, de nombreuses copies peuvent être trouvées sur
Internet, en particulier sur GitHub. Cependant, le dépôt officiel maintenu par les
développeurs se trouve sur https://github.com/owncloud/client.

## Compilation du code source

[Compilation du client](http://doc.owncloud.org/desktop/2.0/building.html)
dans le manuel du client ownCloud.

## Mainteneurs et contributeurs

Les mainteneurs de ce dépôt sont :

* Klaas Freitag <freitag@owncloud.com>
* Daniel Molkentin <danimo@owncloud.com>
* Markus Goetz <guruz@owncloud.com>
* Olivier Goffart <ogoffart@owncloud.com>

Le client pour ordinateur ownCloud  est développé par la communauté ownCloud et
reçoit des correctifs de divers auteurs.

## Rapport de problèmes et contribution

Si vous trouvez des bogues ou si vous avez des suggestions d'amélioration,
veuillez saisir un ticket sur https://github.com/owncloud/client/issues. Ne
contactez pas les auteurs directement par courriel, car cela augmente les
chances que votre rapport soit perdu.

Si vous créez un correctif, veuillez soumettre une [requête de
«  pull » ](https://github.com/owncloud/client/pulls). Pour les correctifs
non triviaux, vous devez signer [l'accord de
contribution](https://owncloud.org/contribute/agreement) avons que nous
puissions accepter votre correctif.

Si vous voulez nous contacter, par exemple avant de commencer le développement
d'une fonctionnalité plus complexe, vous pouvez nous joindre sur
[#owncloud-client-dev](irc://irc.freenode.net/#owncloud-client-dev).

## License

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
    for more details.

## Traduction de la licence

    Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou
    le modifier selon les termes de la licence GNU General Public License
    telle que publiée par la Free Software Foundation ; dans la version 2
    de la licence, ou (si vous le voulez), dans une version ultérieure.

    Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS
    GARANTIE AUCUNE ; sans même la garantie implicite de MARCHANDABILITÉ
    ou D'ADÉQUATION À UN BESOIN PARTICULIER. Consulter la licence GNU
    General Public License pour plus de détails.
