Glossaire
=========

.. glossary::
   :sorted:

   Client de synchronisation ownCloud
   Client ownCloud
     Nom du client de synchronisation ownCloud officiel pour ordinateur, fonctionnant
     sous Windows, Mac OS X et Linux. Il utilise le moteur de synchronisation CSync
     pour la synchronisation avec le serveur ownCloud.

   Serveur ownCloud
     La contre-partie serveur du client ownCloud Client, fourni par la communauté
     ownCloud.

   mtime
   heure de modification
   heure de modification du fichier
     Propriété de fichier utilisée pour déterminer si l'heure du fichier sur le
     serveur ou sur le client est plus récente. Procédure standard dans ownCloud Client
     1.0.5 et versions précédentes, utilisée par ownCloud Client 1.1 et versions
     suivantes seulement quand il n'existe pas de base de données de synchronisation
     et que les fichiers existent déjà dans le répertoire du client.

   identifiant unique
   ETag
     Identifiant assigné à chaque fichier à partir de la version 4.5 du serveur
     ownCloud et soumis par le ``Etag`` HTTP. Utilisé pour vérifier si les fichiers
     sur le client ou le serveur ont changé.
