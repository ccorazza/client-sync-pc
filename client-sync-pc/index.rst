.. _contents:

Manuel du client de synchronisaton ownCloud
===========================================

.. toctree::
   :maxdepth: 2

   introduction
   installing
   navigating
   advancedusage
   autoupdate
   building 
   architecture
   troubleshooting
   faq
   glossary
   
