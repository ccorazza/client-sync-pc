FAQ
===

**Problème :**

Certains fichiers sont téléversés continuellement sur le serveur même s'ils ne
sont pas modifiés.

**Résolution :**

Il est possible qu'un autre programme change la date de modification du fichier.

Si le fichier utilise l'extension ``.eml``, Windows change automatiquement et
continuellement le fichier à moins de supprimer la clé
``\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\PropertySystem\PropertyHandlers``
de la base de registre de Windows.

Consulter http://petersteier.wordpress.com/2011/10/22/windows-indexer-changes-modification-dates-of-eml-files/
pour plus d'informations.
